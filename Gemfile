source 'https://rubygems.org'

# Rails Framework Version
gem 'rails', '~> 4.1.0'

# Database Adapter
gem 'mysql2'

# Assets
gem 'sass-rails'
gem 'haml-rails'
gem 'simple_form'
gem 'uglifier'
gem 'headjs-rails'

# Javascript Libraries
gem 'jquery-rails'
gem 'turbolinks'
gem 'jquery-turbolinks'
gem 'nprogress-rails'

# CoffeeScript
# Not needed in production if precompiling assets
gem 'coffee-rails'

# Frontend Framework
gem 'bootstrap-sass'

# Email Templates styling
gem 'premailer-rails'

# Authentication
gem 'devise'
gem 'cancan'
gem 'omniauth'
gem 'omniauth-facebook'
gem 'omniauth-twitter'
# gem 'omniauth-persona'
# gem 'omniauth-google-oauth2'
# gem 'omniauth-linkedin'

# Admin Tool
# gem 'rails_admin'
gem 'activeadmin', github: 'gregbell/active_admin'

# Utils
gem 'addressable'
gem 'settingslogic'
gem "rolify"

group :development do
  # Docs
  gem 'sdoc', require: false    # bundle exec rake doc:rails

  # Errors
  gem 'better_errors'
  # gem 'binding_of_caller'     # extra features for better_errors
  # gem 'meta_request'          # for rails_panel chrome extension

  # Deployment
  # gem 'capistrano'
  gem 'rvm-capistrano'
  gem 'net-ssh', '2.7.0'

end

group :development, :test do
  # Use spring or zeus
  gem 'spring'                  # keep application running in the background

  # Debugging
  gem 'pry'                     # better than irb
  gem 'coolline'              # sytax highlighting as you type
  gem 'coderay'               # use with coolline
  gem 'awesome_print'           # pretty pring debugging output

  # Testing
  gem 'rspec-rails'
  gem 'factory_girl_rails'
  gem 'ffaker'

  # Mute asset pipeline messages in logs
  gem 'quiet_assets'
  gem 'binding_of_caller'
end

group :test do
  # Helpers
  gem 'shoulda-matchers'
  gem 'database_cleaner'
end

group :production do
  gem 'rails_12factor'          # https://devcenter.heroku.com/articles/rails4
end

