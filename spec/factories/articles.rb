# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :article do
    keywords "MyString"
    content "MyString"
    ordered "2014-06-24"
  end
end
