# Route prefixes use a single letter to allow for vanity urls of two or more characters
Rails.application.routes.draw do
    
  root 'pages#home'

  ActiveAdmin.routes(self)
  # Devise
  devise_prefix = Rails.application.config.auth.devise.path_prefix
  devise_for :users, path: devise_prefix,
    controllers: {registrations: 'users/registrations', sessions: 'users/sessions',
      passwords: 'users/passwords', confirmations: 'users/confirmations', unlocks: 'users/unlocks'},
    path_names: {sign_up: 'signup', sign_in: 'login', sign_out: 'logout'}
  devise_scope :user do
    get "#{devise_prefix}/after" => 'users/registrations#after_auth', as: 'user_root'
  end
  get devise_prefix => redirect('/#account')
  post devise_prefix => redirect('/#account')

  # Static pages
  match '/error' => 'pages#error', via: [:get, :post], as: 'error_page'
  get '/terms' => 'pages#terms', as: 'terms'
  get '/privacy' => 'pages#privacy', as: 'privacy'
  get '/billing' => 'pages#billing', as: 'billing'
  get '/cookies' => 'pages#cookies', as: 'cookies'
  get '/copyright' => 'pages#copyright', as: 'copyright'
  get '/earnings' => 'pages#earnings', as: 'earnings'
  get '/spam' => 'pages#spam', as: 'spam'

  # OAuth
  oauth_prefix = Rails.application.config.auth.omniauth.path_prefix
  get "#{oauth_prefix}/:provider/callback" => 'users/oauth#create'
  get "#{oauth_prefix}/failure" => 'users/oauth#failure'
  get "#{oauth_prefix}/:provider" => 'users/oauth#passthru', as: 'provider_auth'
  get oauth_prefix => redirect("#{oauth_prefix}/login")

  # User
  resources :users, path: 'u', only: :show do
    resources :authentications, path: 'accounts'
  end
  get '/dashboard' => 'articles#index', as: 'user_home'

  # Dummy preview pages for testing.
  get '/p/test' => 'pages#test', as: 'test'
  get '/p/email' => 'pages#email' if ENV['ALLOW_EMAIL_PREVIEW'].present?

  get 'robots.:format' => 'robots#index'

  resources :articles do 
   member do
       get :download
       get :archive
     end
     collection do 
      get :index_archive
     end
  end

  resources :users
  
end
