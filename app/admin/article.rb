ActiveAdmin.register Article do
  # for default asc order
   # config.sort_order = "id_asc"
   filter :keywords
   filter :user, :as => :select, :collection => proc { User.where(:user_type=>"standard") }
   filter :writer, :as => :select, :collection => proc { User.where(:user_type=>"writer").where(:approved=>true) }
   filter :ordered


   # overriding controller method update according to our need
    controller do
      # This code is evaluated within the controller class
      def update 
        unless current_user.is_admin?
          if params[:commit]=="Update&Publish"
              params[:article][:status]="complete"
              params[:article][:published]=true
          else
            params[:article][:status]="inprogress"
            params[:article][:published]=false
          end
        else         
          if params[:article][:status]== "new"&&params[:article][:writer_id]!=""
            params[:article][:status]="assigned"
          end
        end
        super        
      end
    end

   form do |f|
       f.inputs "Article" do
        f.input :user,:as=>:select,:collection=>User.where(user_type: "standard"), :input_html => { :style =>"background-color:#000000;color:white",:disabled=>true } 
         # if can? :assign_writer ,@user
           f.input :writer,:as => :select,:collection => User.where(user_type: "writer"), :input_html => { :style =>(can? :assign_writer ,@user)? "" : "background-color:#000000;color:white" ,:disabled=>(can? :assign_writer ,@user) ? false : true  } 
          # end

           f.input :keywords
           f.input :content
           f.input :ordered,:input_html => { :readonly=>true }
           f.input :exact_kewords,:input_html => { :readonly=>true }
           f.input :article_instruction,:input_html => { :readonly=>true }
           f.input :word_count,:input_html => { :readonly=>true }
           # f.input :user_id,:input_html => { :readonly=>true }
           f.input :published,:input_html=>{:readonly=>true}
           f.input :status,:as => :select,:collection =>["incomplete","complete","inprogress","new","assigned"],:input_html => { :style =>"background-color:#000000;color:white",:disabled=>true } 
           f.input :status,:as => :select,:collection =>["incomplete","complete","inprogress","new","assigned"],:input_html => { :style =>"display:none;" } 
           f.input :specific_style,:input_html => { :readonly=>true }
           f.input :depth_research,:input_html => { :readonly=>true }
           f.input :fixed_delivery_time,:input_html => { :readonly=>true }
           f.input :no_of_article,:input_html => { :readonly=>true }
       end
         f.actions do
             f.action :submit
            if !current_user.admin?
             f.action :submit, :label => "Update&Publish", :button_html=>{:class=>"cancel"}
            end
             f.action :cancel,:button_html => { :class=>"btn btn-default active " }  
         end 
   end
   scope :new_requested,:if => proc{ current_user.admin? }
    scope :unassigned,:if => proc{ current_user.admin? }
   scope :assigned
   scope :inprogress
   scope :completed
  #member action to publish articles
   member_action :publish, method: 'get' do
     article = Article.find(params[:id])
     article.status="complete"
     article.published = true
     if article.save!
     flash[:notice] = "Article published Successfully!"
     end
      redirect_to :back  #this ensures any current filter stays active
   end
 
  member_action :sample, method: 'get' do
     article = Article.find(params[:id])
     article.sample = true
     if article.save!
     flash[:notice] = "Article Successfully Marked as sample!"
     end
      redirect_to :back  #this ensures any current filter stays active
   end
   member_action :unmark_sample, method: 'get' do
   article = Article.find(params[:id])
   article.sample = false
   if article.save!
   flash[:notice] = "Article Successfully Un Marked!"
   end
    redirect_to :back  #this ensures any current filter stays active
 end
   # action_item :only => :show do
   #     link_to "sddsfsdfdsf",publish_admin_article_path(article), :class => "member_link delete_link"      
   #   end


  index do
    selectable_column
  column :id ,:sortable=>:id do |article|
    link_to article.id,[:admin,article]
  end
  column :keywords
  column :content
  column :writer
  column :ordered
  column :article_instruction
  column :no_of_article    
  column :status  
  column :published  

     actions do |article|
       if !article.published?&& !current_user.admin?
        link_to I18n.t('active_admin.publish'),publish_admin_article_path(article), :class => "member_link delete_link"
       end
       if current_user.admin? && !article.sample? && article.is_complete?
        link_to I18n.t('active_admin.sample'),sample_admin_article_path(article), :class => "member_link delete_link"
       elsif current_user.admin? && article.sample? && article.is_complete?
        link_to I18n.t('active_admin.undo_sample'),unmark_sample_admin_article_path(article), :class => "member_link delete_link"
       end
  end
end
actions :all,:except=>[:new]

  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  
  # belongs_to :writer ,:class_name => "User"
  # scope_to :current_user ,:if => proc{  }
   # scope :article_assigned
  # scope_to :current_user ,:if => proc{ Article.where("writer_id",current_user.id) }

  # filter :writer_id, :as => :string
  # scope :article_assigned
  
  # actions :all, :except => [:new]
  permit_params :keywords, :content, :ordered, :exact_kewords, :article_instruction, :no_of_article, :word_count, :fixed_delivery_time, :writer_stars, :depth_research, :specific_style, :article_topic_warning, :status, :assigned_writerm, :user_id, :writer_id, :published,:sample



  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end
  
end
