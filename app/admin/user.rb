ActiveAdmin.register User do

filter :first_name
filter :last_name
filter :approved
filter :email
filter :user_type,:as=>:select,:collection=>["writer","standard"]
 

  form do |f|
      f.inputs "Member Details" do
          f.input :first_name
          f.input :last_name
          f.input :email
          f.input :approved
          f.input :user_type,:as=>:select,:collection=>["writer","standard"],:input_html => { :readonly=>true }
      end
       f.actions 
  end
  #Member action to approve User
  member_action :approve, method: 'get' do
    user = User.find(params[:id])
    user.approved = true
    if user.save!
    flash[:notice] = "Writer Approved Successfully!"
    # redirect_to action: :index
    end
     redirect_to :back  #this ensures any current filter stays active
  end
  #Member action to unapprove User
  member_action :unapprove, method: 'get' do
    user = User.find(params[:id])
    user.approved = false
    if user.save!
    flash[:notice] = "Writer Unapproved Successfully!" 
    end
    redirect_to :back  #this ensures any current filter stays active
  end
  #Member action to show on "show page" of user
  action_item :only => :show do
   if user.approved? && user.is_writer?
         link_to I18n.t('active_admin.cancle'),unapprove_admin_user_path(user), :class => "member_link delete_link"      
        elsif !user.approved? && user.is_writer?
         link_to I18n.t('active_admin.approve'), approve_admin_user_path(user), :class => "member_link edit_link"
        end  
  end
# index action of user controller 
  index do
    selectable_column
    column :id ,:sortable=>:id do |user|
      link_to article.id,[:admin,user]
    end     
    column :first_name
    column :last_name
    column :email
    column :approved
    # column :user_type
    actions do |user|
      if user.approved? && user.is_writer?
       link_to I18n.t('active_admin.cancle'),unapprove_admin_user_path(user), :class => "member_link delete_link"      
      elsif !user.approved? && user.is_writer?
       link_to I18n.t('active_admin.approve'), approve_admin_user_path(user), :class => "member_link edit_link"
      end     
    end
  end
  actions :all
  # Only  show menu if current_user is admin
  menu :if => proc{ current_user.admin? }
  # Only give access if current_user is admin
   # scope_to :current_user ,:unless => proc{ current_user.admin? }

 
  scope :admin
  scope :standard_user
  scope :approved_writer
  scope :unapproved_writer
  permit_params :first_name,:last_name,:user_type,:approved



  # batch_action :activate_writer_only do |selection|
  #   User.find(selection).each do |v|
  #     # v.active = true
  #     # v.save
  #   end
  #   # redirect_to :back  #this ensures any current filter stays active
  # end
  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or        
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end
  
end
