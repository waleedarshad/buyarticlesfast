class Article < ActiveRecord::Base
		
	belongs_to :user
	belongs_to :writer ,:class_name => "User",:foreign_key => "writer_id"

	 scope :unassigned, -> {where("writer_id IS NULL")}
	 scope :assigned, -> {where("writer_id IS NOT NULL")}
	 scope :inprogress, -> {assigned.where(:status=>"inprogress")}
	 scope :completed, -> {assigned.where(:status=>"complete")}
	 scope :new_requested, -> {where(:status=>"new")}


	 after_create :mark_as_new

	# scope :writer, -> { where(user_type: 'writer') }
	# scope :standard_user, -> { where(user_type: 'standard') }
	# scope :approved_writer, -> { writer.where(:approved=>true)} 
	# scope :unapproved_writer,->{ writer.where(:approved=>false)}
	
	def mark_as_new
		self.update(:status=>"new")
	end

	def status_enum
		[['new', 0],['Pending',1],['InProgress',2],['Completed',3]]
	end
	def is_complete?
		self.status == "complete" ? true : false
	end

end
