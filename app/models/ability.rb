# CanCan Abilities
#
# See the wiki for details:
# https://github.com/ryanb/cancan/wiki/Defining-Abilities

class Ability
  include CanCan::Ability
  def initialize(user = User.new)
  can :read, ActiveAdmin::Page, :name => "Dashboard"

    if user.is_admin?	
      # can :manage, User 
      can :manage, :all
      can :read, ActiveAdmin::Page, :name => "Dashboard"
    end 	
    if user.is_writer?
      can :manage, User , :id=>user.id
      cannot :assign_writer, User,:id=>user.id
  	  can :read, Article,:writer_id => user.id
      can [:show, :edit, :update], Article,:writer_id => user.id
    end
   end
end
