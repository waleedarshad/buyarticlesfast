class ArticlesController < ApplicationController
  skip_authorization_check
  
  def index
    @articles = current_user.articles.where("published= ?",true)
  end

  def new
     @request_article = current_user.articles.build
  end

  def create
    @request_article = current_user.articles.build(request_article_params)
    if @request_article.save
      flash[:notice]="Request successfully submit"
      redirect_to action: :index
    end
  end

  def update
  end
 
  def show
    @article = current_user.articles.find_by_id(params[:id])
  end

  def destroy
  end

  def download
    article = Article.find(params[:id])
    # file = File.new("./public/article_files"+article.keywords+".txt",'w')
    # file.puts article.content
    # file.close
    send_data article.content, :filename => article.keywords+".txt"
    # redirect_to action: :index
  end
  def archive
     article = Article.find(params[:id])
     article.archive = true
     if article.save!
     redirect_to articles_path,:notice=>"Article Archives Successfully"
     end
  end
  def index_archive
    @article_archive = Article.where(:archive=>true)
  end
  private
  def request_article_params
    params.require(:article).permit(:keywords, :content, :ordered, :exact_kewords, :article_instruction, :no_of_article, :word_count, :fixed_delivery_time, :writer_stars, :depth_research, :specific_style, :article_topic_warning, :status, :assigned_writerm, :user_id, :writer_id)
  end
end
