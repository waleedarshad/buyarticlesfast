class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string    :keywords
      t.text      :content
      t.date      :ordered
      t.boolean   :exact_kewords
      t.text      :article_instruction
      t.integer   :no_of_article
      t.integer   :word_count
      t.boolean   :fixed_delivery_time
      t.boolean   :writer_stars
      t.boolean   :depth_research
      t.boolean   :specific_style
      # t.boolean  :terms_and_conditions
      t.boolean   :article_topic_warning
      t.string    :status
      t.string    :assigned_writer
      t.integer   :user_id
      t.integer   :writer_id

      t.timestamps
    end
  end
end
